import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { UserUseCaseModule } from '@root/user-uc/user-use-case.module';
import { AuthResolver } from '@root/auth/auth.resolver';
import { GoogleOauthModule } from '@root/auth/google/google-oauth.module';
import { PubSubModule } from '@root/pubsub/pub-sub.module';

@Module({
  imports: [
    PubSubModule,
    UserUseCaseModule,
    GoogleOauthModule,
    JwtModule.registerAsync({
      imports: undefined,
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          global: true,
          secret: config.get<string>('JWT_SECRET'),
          signOptions: { expiresIn: '4d' },
        };
      },
    }),
  ],
  providers: [AuthResolver],
  exports: [AuthResolver],
})
export class AuthModule {}
