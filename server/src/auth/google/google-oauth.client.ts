import { OAuth2Client } from 'google-auth-library';
import { Injectable, Scope } from '@nestjs/common';

// DEFAULT scope makes singleton class instances
@Injectable({
  scope: Scope.DEFAULT,
})
export class GoogleOauthClient {
  readonly client: OAuth2Client;
  constructor(clientID: string, clientSecret: string) {
    this.client = new OAuth2Client(clientID, clientSecret);
  }
}
