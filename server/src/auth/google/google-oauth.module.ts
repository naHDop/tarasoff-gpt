import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { GoogleOauthClient } from '@root/auth/google/google-oauth.client';
import { GoogleOauthService } from '@root/auth/google/google-oauth.service';

@Module({
  imports: [ConfigModule],
  providers: [
    {
      provide: GoogleOauthClient,
      inject: [ConfigService],
      useFactory(configService: ConfigService): GoogleOauthClient {
        return new GoogleOauthClient(
          configService.get<string>('OAUTH_GOOGLE_CLIENT_ID'),
          configService.get<string>('OAUTH_GOOGLE_CLIENT_SECRET'),
        );
      },
    },
    GoogleOauthService,
  ],
  exports: [GoogleOauthService],
})
export class GoogleOauthModule {}
