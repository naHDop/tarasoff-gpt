import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { GoogleOauthClient } from '@root/auth/google/google-oauth.client';
import { TokenPayload } from 'google-auth-library/build/src/auth/loginticket';

@Injectable()
export class GoogleOauthService {
  constructor(
    private readonly googleOAuthClient: GoogleOauthClient,
    private readonly configService: ConfigService,
  ) {}

  async verify(tokenId: string): Promise<TokenPayload | undefined> {
    const ticket = await this.googleOAuthClient.client.verifyIdToken({
      idToken: tokenId,
      audience: this.configService.get<string>('OAUTH_GOOGLE_CLIENT_ID'),
    });

    return ticket.getPayload();
  }
}
