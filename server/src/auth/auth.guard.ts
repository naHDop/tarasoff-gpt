import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly jwtService: JwtService) {}
  canActivate(context: ExecutionContext): boolean {
    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getContext().req;
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }

    const data = this.jwtService.verify(token);
    return !!data.id;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const AuthHeader: string = request.headers['x-auth-token'] as string;
    const [type, token] = AuthHeader ? AuthHeader.split(' ') : [];
    return type === 'Bearer' ? token.replace('Bearer ', '') : undefined;
  }
}
