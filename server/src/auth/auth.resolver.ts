import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { InternalServerErrorException, UseGuards } from '@nestjs/common';

import { UserUseCaseService } from '@root/user-uc/user-use-case.service';
import { GoogleSignInInput, UserView } from '@root/graphql.schema';
import { GoogleOauthService } from '@root/auth/google/google-oauth.service';
import { PubSubProvider } from '@root/pubsub/pub-sub.provider';
import { AuthGuard } from '@root/auth/auth.guard';
import { CtxUser, UserId } from '@root/auth/user-id.decorator';

@Resolver('Auth')
export class AuthResolver {
  constructor(
    private readonly userUseCaseService: UserUseCaseService,
    private readonly googleOAuthService: GoogleOauthService,
    private readonly jwtService: JwtService,
    private readonly pubSubProvider: PubSubProvider,
  ) {}

  @Query('user')
  @UseGuards(AuthGuard)
  async findOneById(@UserId() ctxUser: CtxUser): Promise<UserView> {
    const user = await this.userUseCaseService.getUserById(ctxUser.id);
    return {
      email: user.email,
      createdAt: user.createdAt.toISOString(),
      updatedAt: user.updatedAt.toISOString(),
      hashedPassword: user.hashedPassword,
      id: user.id,
      imageSrc: user.imageSrc,
      firstName: user.firstName,
    };
  }

  @Mutation('googleSingIn')
  async googleSignIn(
    @Args('signInInput') args: GoogleSignInInput,
  ): Promise<string> {
    const creds = await this.googleOAuthService.verify(args.tokenId);
    if (!creds) {
      throw new InternalServerErrorException(
        'no credential after token verified',
      );
    }

    let userId: string;
    try {
      const user = await this.userUseCaseService.getUserByEmail(creds.email);
      userId = user.id;
    } catch {
      userId = await this.userUseCaseService.createUser({
        email: creds.email,
        firstName: creds.name || creds.given_name,
        imageSrc: creds.picture,
      });
    }

    this.pubSubProvider.pubsub.publish('userCreated', {
      userCreated: { userId },
    });

    return this.jwtService.sign(
      {
        id: userId,
      },
      { issuer: creds.iss },
    );
  }

  @Subscription('userCreated', {
    filter: (payload, variables) => payload.userCreated.userId === variables.id,
  })
  async userCreated(@Args('id') userId: string) {
    return this.pubSubProvider.pubsub.asyncIterator('userCreated');
  }
}
