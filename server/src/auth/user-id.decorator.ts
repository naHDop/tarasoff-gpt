import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';

export interface CtxUser {
  id: string;
  iat: number;
  exp: number;
  iss: string;
}

export const UserId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const context = GqlExecutionContext.create(ctx);
    const request = context.getContext().req;
    const Authorization = request.get('Authorization');
    const token = Authorization.replace('Bearer ', '');

    const jwtService = new JwtService({ secret: process.env.JWT_SECRET_KEY });

    const verifiedData = jwtService.verify<CtxUser>(token);

    if (!verifiedData) {
      throw new UnauthorizedException('No token available');
    }

    return verifiedData.id;
  },
);
