import {
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
  IsOptional,
} from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsString()
  firstName: string;

  @IsString()
  imageSrc: string;

  @IsString()
  @IsOptional()
  @MinLength(5)
  @MaxLength(18)
  hashedPassword?: string;
}
