import { CreateUserDto } from '@root/domains/User/dtos/create-user.dto';
import { UserEntity } from '@root/repository/user/user.entity';
import { CreateUserUseCaseDto } from '@root/user-uc/dtos/create-user.dto';

export interface IUser {
  id: string;
  email: string;
  firstName: string;
  imageSrc: string;
  createdAt: Date;
  updatedAt?: Date;
  hashedPassword: string;
}

export interface IUserRepository {
  createUser(dto: CreateUserDto): Promise<UserEntity>;
  getUserById(userId: string): Promise<UserEntity>;
  getUserByEmail(email: string): Promise<UserEntity>;
}

export interface IUserUseCase {
  createUser(dto: CreateUserUseCaseDto): Promise<string>;
  getUserById(id: string): Promise<IUser>;
  getUserByEmail(email: string): Promise<IUser>;
}
