import { Injectable, Scope } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';

@Injectable({
  scope: Scope.DEFAULT,
})
export class PubSubProvider {
  readonly pubsub: PubSub;
  constructor() {
    this.pubsub = new PubSub();
  }
}
