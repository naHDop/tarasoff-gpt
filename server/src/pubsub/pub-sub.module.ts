import { Module } from '@nestjs/common';
import { PubSubProvider } from '@root/pubsub/pub-sub.provider';

@Module({
  providers: [PubSubProvider],
  exports: [PubSubProvider],
})
export class PubSubModule {}
