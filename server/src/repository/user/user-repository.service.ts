import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { v4 as uuidv4 } from 'uuid';

import { IUserRepository } from '@domains/User/IUser';
import { CreateUserDto } from '@domains/User/dtos/create-user.dto';
import { UserEntity } from '@repository/user/user.entity';

@Injectable()
export class UserRepositoryService implements IUserRepository {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
  ) {}
  async createUser(dto: CreateUserDto): Promise<UserEntity> {
    const applicant = await this.usersRepository.findOne({
      where: { email: dto.email },
    });
    if (applicant) {
      throw new Error('user exists');
    }

    const newUser = this.usersRepository.create();
    newUser.email = dto.email;
    newUser.id = uuidv4();
    newUser.firstName = dto.firstName;
    newUser.imageSrc = dto.imageSrc;
    newUser.hashedPassword = dto.hashedPassword;

    return this.usersRepository.save(newUser);
  }

  async getUserByEmail(email: string): Promise<UserEntity> {
    const user = await this.usersRepository.findOne({ where: { email } });
    if (!user) {
      throw new Error('user not exists');
    }
    return user;
  }

  async getUserById(userId: string): Promise<UserEntity> {
    const user = await this.usersRepository.findOne({ where: { id: userId } });
    if (!user) {
      throw new Error('user not exists');
    }
    return user;
  }
}
