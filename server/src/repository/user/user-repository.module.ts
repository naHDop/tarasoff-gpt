import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserEntity } from '@repository/user/user.entity';
import { UserRepositoryService } from '@repository/user/user-repository.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  providers: [UserRepositoryService],
  exports: [UserRepositoryService, TypeOrmModule],
})
export class UserRepositoryModule {}
