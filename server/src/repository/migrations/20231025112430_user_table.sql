-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS users (
    "id" varchar PRIMARY KEY,
    "email" varchar(128) NOT NULL,
    "password" varchar(256),
    "first_name" varchar(256),
    "image_src" varchar(512),
    "created_at" timestamptz NOT NULL DEFAULT (now()),
    "updated_at" timestamptz
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS users;
-- +goose StatementEnd
