import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UtilsModule } from '@root/utils/utils.module';
import { GoogleOauthModule } from '@root/auth/google/google-oauth.module';
import { UserEntity } from '@repository/user/user.entity';
import { UserRepositoryModule } from '@repository/user/user-repository.module';
import { UserUseCaseModule } from '@root/user-uc/user-use-case.module';
import { upperDirectiveTransformer } from '@root/common/derectives/upper-case.derective';
import { AuthModule } from '@root/auth/auth.module';
import { PubSubModule } from '@root/pubsub/pub-sub.module';
import { OpenAiModule } from '@root/chat-gpt/client/open-ai.module';
import { ChatGptModule } from '@root/chat-gpt/chat-gpt.module';

@Module({
  imports: [
    // Config module initializing
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: '.env',
    }),
    // Database connection configuration
    TypeOrmModule.forRootAsync({
      imports: undefined,
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        type: 'postgres',
        host: config.get<string>('DB_HOST'),
        port: config.get<number>('DB_PORT'),
        username: config.get<string>('POSTGRES_USER'),
        password: config.get<string>('POSTGRES_PASSWORD'),
        database: config.get<string>('POSTGRES_DB'),
        entities: [UserEntity],
        // Run migration on server start
        // migrationsRun: true,
        // migrations: [cwd + '/repository/migrations/*{.ts,.js}'],
        // Auth load relations
        autoLoadEntities: true,
        synchronize: config.get<string>('APP_MODE') === 'dev',
      }),
    }),
    // GraphQL config
    GraphQLModule.forRootAsync({
      driver: ApolloDriver,
      imports: undefined,
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => {
        return {
          csrfPrevention: config.get<string>('APP_MODE') === 'dev',
          typePaths: ['./**/*.graphql', './**/*.gql'],
          path: 'graphql',
          definitions: {
            path: join(process.cwd(), 'src/graphql.schema.ts'),
            outputAs: 'interface',
          },
          playground: config.get<string>('APP_MODE') === 'dev',
          transformSchema: (schema) =>
            upperDirectiveTransformer(schema, 'upper'),
          subscriptions: {
            'graphql-ws': {
              path: 'subscription',
            },
            'subscriptions-transport-ws':
              config.get<string>('APP_MODE') === 'dev',
          },
        } as ApolloDriverConfig;
      },
    }),
    UtilsModule,
    GoogleOauthModule,
    UserRepositoryModule,
    UserUseCaseModule,
    AuthModule,
    PubSubModule,
    OpenAiModule,
    ChatGptModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
