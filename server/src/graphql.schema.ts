
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export enum ChatRole {
    user = "user",
    assistant = "assistant",
    system = "system"
}

export interface GoogleSignInInput {
    tokenId: string;
}

export interface UserPrompt {
    chatId?: Nullable<string>;
    content: string;
}

export interface UserId {
    userId: string;
}

export interface Chat {
    id: string;
    title: string;
    messages: Nullable<Message>[];
    createdAt: string;
}

export interface ChatStream {
    chatId: string;
    content: string;
}

export interface Message {
    id: string;
    role: ChatRole;
    content: string;
    createdAt: string;
}

export interface ChatId {
    chatId: string;
}

export interface IQuery {
    user(): UserView | Promise<UserView>;
    history(withMessages: boolean): Nullable<Chat>[] | Promise<Nullable<Chat>[]>;
    chat(chatId: string): Chat | Promise<Chat>;
}

export interface IMutation {
    googleSingIn(signInInput?: Nullable<GoogleSignInInput>): string | Promise<string>;
    conversation(conversationInput?: Nullable<UserPrompt>): ChatId | Promise<ChatId>;
}

export interface ISubscription {
    userCreated(id: string): UserId | Promise<UserId>;
    chatStream(id: string): ChatStream | Promise<ChatStream>;
}

export interface UserView {
    id: string;
    email: string;
    firstName: string;
    imageSrc: string;
    hashedPassword?: Nullable<string>;
    createdAt: string;
    updatedAt?: Nullable<string>;
}

type Nullable<T> = T | null;
