import {
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
  IsOptional,
} from 'class-validator';

export class CreateUserUseCaseDto {
  @IsEmail()
  email: string;

  @IsString()
  firstName: string;

  @IsString()
  imageSrc: string;

  @IsString()
  @MinLength(5)
  @MaxLength(18)
  @IsOptional()
  password?: string;
}
