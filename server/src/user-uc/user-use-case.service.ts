import { Injectable } from '@nestjs/common';

import { IUser, IUserUseCase } from '@domains/User/IUser';
import { HashService } from '@root/utils/hash.service';
import { CreateUserUseCaseDto } from '@root/user-uc/dtos/create-user.dto';
import { UserRepositoryService } from '@repository/user/user-repository.service';

@Injectable()
export class UserUseCaseService implements IUserUseCase {
  constructor(
    private readonly userRepository: UserRepositoryService,
    private readonly hashService: HashService,
  ) {}

  async createUser(dto: CreateUserUseCaseDto): Promise<string> {
    let hashedPassword;
    if (dto.password) {
      hashedPassword = await this.hashService.generateHash(dto.password);
    }
    const user = await this.userRepository.createUser({
      email: dto.email,
      firstName: dto.firstName,
      imageSrc: dto.imageSrc,
      hashedPassword,
    });
    return user.id;
  }

  async getUserByEmail(email: string): Promise<IUser> {
    const user = await this.userRepository.getUserByEmail(email);
    return {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      imageSrc: user.imageSrc,
      hashedPassword: user.hashedPassword,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    };
  }

  async getUserById(id: string): Promise<IUser> {
    const user = await this.userRepository.getUserById(id);
    return {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      imageSrc: user.imageSrc,
      hashedPassword: user.hashedPassword,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    };
  }
}
