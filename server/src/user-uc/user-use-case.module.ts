import { Module } from '@nestjs/common';

import { UserUseCaseService } from '@root/user-uc/user-use-case.service';
import { UserRepositoryModule } from '@repository/user/user-repository.module';
import { UtilsModule } from '@root/utils/utils.module';

@Module({
  imports: [UtilsModule, UserRepositoryModule],
  providers: [UserUseCaseService],
  exports: [UserUseCaseService],
})
export class UserUseCaseModule {}
