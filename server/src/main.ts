import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const config: ConfigService = app.get(ConfigService);

  app.enableShutdownHooks();
  app.enableCors({
    origin: config.get<string>('ORIGIN'),
    methods: ['POST'],
    credentials: true,
    allowedHeaders: [
      'Access-Control-Allow-Headers',
      'Accept',
      'Connection',
      'Host',
      'Content-Length',
      'Accept-Encoding',
      'X-Auth-Token',
      'Content-Type',
      'X-CSRF-Token',
      'Referer',
      'User-Agent',
      'Origin',
    ],
  });

  await app.listen(config.get<string>('APP_PORT'));
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
