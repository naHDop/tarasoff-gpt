import { Injectable, Scope } from '@nestjs/common';
import { OpenAI } from 'openai';
import { ChatCompletionMessageParam } from 'openai/src/resources/chat/completions';
import { APIPromise } from 'openai/core';
import { Stream } from 'openai/streaming';
import { ChatCompletion, ChatCompletionChunk } from 'openai/resources';

@Injectable({
  scope: Scope.DEFAULT,
})
export class OpenAiProvider {
  readonly client: OpenAI;
  constructor(apiKey: string, organization: string) {
    this.client = new OpenAI({
      apiKey,
      organization,
      timeout: 20 * 1000,
    });
  }

  createCompletionStream(
    messages: Array<ChatCompletionMessageParam>,
  ): APIPromise<Stream<ChatCompletionChunk>> {
    return this.client.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages,
      stream: true,
    });
  }

  createCompletion(
    messages: Array<ChatCompletionMessageParam>,
  ): APIPromise<ChatCompletion> {
    return this.client.chat.completions.create({
      model: 'gpt-3.5-turbo',
      messages,
    });
  }
}
