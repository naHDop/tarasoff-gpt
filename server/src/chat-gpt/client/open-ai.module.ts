import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { OpenAiProvider } from '@root/chat-gpt/client/open-ai.provider';

@Module({
  imports: [ConfigModule],
  providers: [
    {
      provide: OpenAiProvider,
      inject: [ConfigService],
      useFactory(configService: ConfigService): OpenAiProvider {
        return new OpenAiProvider(
          configService.get<string>('OPEN_AI_KEY'),
          configService.get<string>('OPEN_AI_ORGANIZATION_ID'),
        );
      },
    },
  ],
  exports: [OpenAiProvider],
})
export class OpenAiModule {}
