import { Args, Mutation, Resolver, Subscription } from '@nestjs/graphql';

import { OpenAiProvider } from '@root/chat-gpt/client/open-ai.provider';
import { PubSubProvider } from '@root/pubsub/pub-sub.provider';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@root/auth/auth.guard';
import { ChatId, ChatRole, UserPrompt } from '@root/graphql.schema';

@Resolver('ChatGpt')
export class ChatGptResolver {
  constructor(
    private readonly openAiProvider: OpenAiProvider,
    private readonly pubSubProvider: PubSubProvider,
  ) {}

  @Mutation('conversation')
  @UseGuards(AuthGuard)
  async conversation(
    @Args('conversationInput') convInput: UserPrompt,
  ): Promise<ChatId> {
    const stream = await this.openAiProvider.createCompletionStream([{ role: ChatRole.user, content: convInput.content }]);
    let text = '';

    for await (const part of stream) {
      text += part.choices[0]?.delta?.content || '';
      this.pubSubProvider.pubsub.publish('chatStream', {
        chatStream: {
          chatId: convInput.chatId,
          content: text,
        },
      });
    }

    // TODO: save text
    console.log('text', text);

    return { chatId: convInput.chatId };
  }

  @Subscription('chatStream', {
    filter: (payload, variables) => payload.chatStream.chatId === variables.id,
  })
  async chatStream(@Args('id') chatId: string) {
    return this.pubSubProvider.pubsub.asyncIterator('chatStream');
  }
}
