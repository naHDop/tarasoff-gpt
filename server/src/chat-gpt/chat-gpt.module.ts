import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { ChatGptResolver } from '@root/chat-gpt/chat-gpt.resolver';
import { OpenAiModule } from '@root/chat-gpt/client/open-ai.module';
import { PubSubModule } from '@root/pubsub/pub-sub.module';

@Module({
  imports: [OpenAiModule, PubSubModule, JwtModule],
  providers: [ChatGptResolver],
  exports: [ChatGptResolver],
})
export class ChatGptModule {}
