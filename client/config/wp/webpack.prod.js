const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");
const path = require("path");

const cwd = process.cwd();

module.exports = merge(common, {
  mode: "production",
  devtool: "inline-source-map",
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(cwd, "build"),
  },
});
