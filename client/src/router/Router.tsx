import React from "react";
import { createBrowserRouter } from "react-router-dom";
import { Link, Stack, Typography } from "@mui/joy";

import { App } from '@root/App'
import { SignIn } from "@features/GoogleSignIn/View";
import { VerticalSide } from "@root/components/layout/VerticalSide";

export const router = createBrowserRouter([
    {
        index: true,
        element: <h1>Main</h1>,
        errorElement: <div>Page not found</div>,
    },
    {
        path: "dashboard",
        element: <App />,
        errorElement: <div>Page not found</div>,
        children: [
            {
                index: true,
                element: <div>TEST CHILDREN</div>
            }
        ]
    },
    {
        path: "sign-in",
        element: (
            <VerticalSide
                logoTitle="Tarasoff Gpt"
            >
                <Stack gap={4} sx={{ mb: 2 }}>
                    <Stack gap={1}>
                        <Typography level="h3">Sign in</Typography>
                        <Typography level="body-sm">
                            New to company?{' '}
                            <Link href="#replace-with-a-link" level="title-sm">
                                Sign up!
                            </Link>
                        </Typography>
                    </Stack>
                    <SignIn />
                </Stack>
            </VerticalSide>
        )
    }
]);
