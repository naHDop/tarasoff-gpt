import React, { StrictMode } from 'react';
import {
    RouterProvider,
} from "react-router-dom";
import ReactDOM from 'react-dom/client';
import { GoogleOAuthProvider } from "@react-oauth/google";
import { ApolloProvider } from '@apollo/client';
import { ToastContainer } from 'react-toastify';
import { CssBaseline, CssVarsProvider } from "@mui/joy";
import 'react-toastify/dist/ReactToastify.css';

import { router } from '@root/router';
import { apolloClient } from "@root/client/gql/apollo";

const root = ReactDOM.createRoot(document.querySelector('#root') as HTMLElement);
// @ts-ignore
const googleOAuthClientId: string = process.env.OAUTH_GOOGLE_CLIENT_ID

root.render(
    <StrictMode>
        <GoogleOAuthProvider clientId={googleOAuthClientId}>
            <ApolloProvider client={apolloClient} >
                <CssVarsProvider defaultMode="dark" disableTransitionOnChange>
                    <CssBaseline />
                    <ToastContainer autoClose={3000} hideProgressBar />
                    <RouterProvider router={router} />
                </CssVarsProvider>
            </ApolloProvider>
        </GoogleOAuthProvider>
    </StrictMode>
);
