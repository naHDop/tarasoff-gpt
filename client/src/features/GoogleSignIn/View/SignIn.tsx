import React, { useEffect } from 'react';

import { GoogleSignIn } from "@ui/GoogleSignIn";
import { useGoogleSignInModel } from "@features/GoogleSignIn/Controller";
import { toast } from "react-toastify";

export function SignIn() {
    const { signIn, data, error } = useGoogleSignInModel()

    useEffect(() => {
        if (data) {
            localStorage.setItem('token', data.googleSingIn)
        }
        if (error) {
            toast(error.message, { type: 'error' })
        }
    }, [data, error])

    return (
        <GoogleSignIn
            onSuccess={async (token) => {
                if (token.credential) {
                    signIn(token.credential)
                } else {
                    toast('no credentials from google', { type: 'error' })
                }
            }}
            onError={() => {
                console.log("Login Failed");
            }}
        />
    );
}
