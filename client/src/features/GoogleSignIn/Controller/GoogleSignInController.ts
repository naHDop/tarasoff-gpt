import { useCallback } from "react";
import { useMutation } from '@apollo/client';

import { GOOGLE_SIGN_IN } from "@features/GoogleSignIn/Driver/mutations";

export function useGoogleSignInModel() {
    const [signInWithGoogle, { data, loading, error }] = useMutation<
        { googleSingIn: string },
        { signInInput: { tokenId: string } }
    >(GOOGLE_SIGN_IN);

    const signIn = useCallback((tokenId: string) => {
        return signInWithGoogle({ variables: { signInInput: { tokenId } } })
    }, [useMutation])

    return {
        loading,
        error,
        data,
        signIn,
    }
}