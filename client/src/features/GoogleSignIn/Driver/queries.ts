import { gql } from "@apollo/client";

const GET_USER = gql`
    query GetUserById {
        user {
            id
            firstName
            email
            imageSrc
        }
    }
`;