import { gql } from '@apollo/client';

export const GOOGLE_SIGN_IN = gql`
    mutation GoogleSignInInput($signInInput: GoogleSignInInput) {
        googleSingIn(signInInput: $signInInput)
    }
`;
