import React from 'react';
import { useColorScheme } from "@mui/joy";

enum LinkedInProfileSize {
    Small = "small",
    Large = "large",
    Medium = "medium",
}

enum LinkedInProfileOrientation {
    Horizontal = "HORIZONTAL",
    Vertical = "VERTICAL",
}

interface ILinkedInProfileProps {
    size?: LinkedInProfileSize
    orientation?: LinkedInProfileOrientation
}

export function LinkedInProfile(props: ILinkedInProfileProps) {
    const { size = LinkedInProfileSize.Small, orientation = LinkedInProfileOrientation.Vertical } = props
    const { mode } = useColorScheme();

    return (
        <div 
            className="badge-base LI-profile-badge" 
            data-locale="en_US" 
            data-size={size} 
            data-theme={mode}
            data-type={orientation} 
            data-vanity="grigory-tarasov" 
            data-version="v1"
        >
            <a
                className="badge-base__link LI-simple-link"
                href="https://me.linkedin.com/in/grigory-tarasov?trk=profile-badge"
            >
                Grigory Tarasov
            </a>
        </div>
    );
}
