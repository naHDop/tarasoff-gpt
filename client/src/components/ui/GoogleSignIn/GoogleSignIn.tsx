import React from 'react';
import { GoogleLoginProps, GoogleLogin } from "@react-oauth/google";
import { useColorScheme } from "@mui/joy";

export function GoogleSignIn(props: GoogleLoginProps) {
    const { mode } = useColorScheme();
    return <GoogleLogin theme={mode === 'dark' ? 'filled_black' : 'outline'} {...props} />
}
